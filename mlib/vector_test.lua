-- TODO figure out stuff on relative imports
local Vector = require('mlib/vector')
local assert = require('mlib/assert')

testSuite("vector", function()
    doTest("create new", function()
        local vec = Vector:new()
        assert.equal(vec:length(), 0)
    end)

    doTest("push() and length()", function()
        local vec = Vector:new()
        vec:push(1)
        assert.equal(vec:length(), 1)
        vec:push(2)
        assert.equal(vec:length(), 2)
        vec:push(3)
        assert.equal(vec:length(), 3)
        assert.equal(vec:length(), #vec)
    end)

    doTest("get()", function()
        local vec = Vector:new()
        vec:push(1)
        vec:push(2)
        vec:push(3)

        assert.equal(vec:get(1), 1)
        assert.equal(vec:get(2), 2)
        assert.equal(vec:get(3), 3)
    end)

    doTest("__index and __newindex", function()
        local vec = Vector:new()
        vec:push(1)
        vec:push(2)
        vec:push(3)

        vec:set(1, 10)
        assert.equal(vec:get(1), 10)
        assert.equal(vec[1], 10)
        
        vec[1] = 15
        assert.equal(vec:get(1), 15)
        assert.equal(vec[1], 15)
    end)

    
    doTest("negative indexes", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        assert.equal(vec:get(-1), 3)
        assert.equal(vec:get(-2), 2)
        assert.equal(vec:get(-3), 15)
    end)

    doTest("pop()", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)
    
        assert.equal(vec:pop(), 3)
        assert.equal(vec:pop(), 2)
        assert.equal(vec:pop(), 15)
        assert.equal(vec:pop(), nil)
        assert.equal(#vec, 0)
    end)
    
    doTest("each()", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        local count = 0
        vec:each(function(i, v)
            assert.equal(count, i - 1)
            count = count + 1
            if (i == 1) then
                assert.equal(v, 15)
            elseif (i == 2) then
                assert.equal(v, 2)
            elseif (i == 3) then 
                assert.equal(v, 3)
            else
                error("invalid index when mapping: " .. i)
            end
        end)
        assert.equal(count, 3)
    end)

    doTest("map()", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        local mapResult = vec:map(function(i, v)
            return v + 5;
        end)
        assert.equal(mapResult:get(1), 20)
        assert.equal(mapResult:get(2), 7)
        assert.equal(mapResult:get(3), 8)
        assert.equal(#mapResult, 3)
    end)
    
    doTest("reduce()", function() 
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        local reduceResult = vec:reduce(function(i, v, result)
            result = result or 0
            return result + v;
        end)
        assert.equal(reduceResult, 3 + 2 + 15)
        
    end)

    doTest("pairs()", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        local count = 0
        for i, v in pairs(vec) do
            assert.equal(count, i - 1)
            count = count + 1
            if (i == 1) then
                assert.equal(v, 15)
            elseif (i == 2) then
                assert.equal(v, 2)
            elseif (i == 3) then 
                assert.equal(v, 3)
            else
                error("invalid index in pairs(): " .. i)
            end
        end
    end)

    doTest("ipairs()", function()
        local vec = Vector:new()
        vec:push(15)
        vec:push(2)
        vec:push(3)

        local count = 0
        for i, v in ipairs(vec) do
            assert.equal(count, i - 1)
            count = count + 1
            if (i == 1) then
                assert.equal(v, 15)
            elseif (i == 2) then
                assert.equal(v, 2)
            elseif (i == 3) then 
                assert.equal(v, 3)
            else
                error("invalid index in ipairs(): " .. i)
            end
        end
    end)

    skipTest("ipairs with array containing nil values")

    doTest("equals()", function()
        local v1 = Vector:new()
        local v2 = Vector:new()

        v1:push(1)
        v2:push(1)
        v1:push(2)
        v2:push(2)
        v1:push(3)
        assert.equal(v1:equals(v2), false)
        v2:push(3)
        assert.equal(v1:equals(v2), true)

        v1[2] = 5
        assert.equal(v1:equals(v2), false)

        v2[2] = 5
        assert.equal(v1:equals(v2), true)
    end)

    doTest("sort()", function()
        local vec = Vector:new()

        vec:push(3)
        vec:push(2)
        vec:push(5)
        vec:push(10)
        vec:push(1)

        vec:sort()

        local expected = Vector:new()
        expected:push(1)
        expected:push(2)
        expected:push(3)
        expected:push(5)
        expected:push(10)
        assert.equal(vec:equals(expected), true)
    end)

    skipTest("sort() with custom fn")

    doTest("of()", function()
        local vec = Vector.of({1,2,3})
        assert.equal(vec[1],1)
        assert.equal(vec[2],2)
        assert.equal(vec[3],3)
        assert.equal(#vec, 3)
    end)

    doTest("of() truncates hashmap", function()
        local vec = Vector.of({1,2,3, foo=5})
        assert.equal(vec[1],1)
        assert.equal(vec[2],2)
        assert.equal(vec[3],3)
        assert.equal(#vec, 3)
    end)

    doTest("join()", function()
        local vec = Vector.of({1,2,3})
        local expected = "1,2,3"
        assert.equal(vec:join(), expected)
    end)

    doTest("join('')", function()
        local vec = Vector.of({1,2,3})
        local expected = "123"
        assert.equal(vec:join(''), expected)
    end)

end)
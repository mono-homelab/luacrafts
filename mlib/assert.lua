local assert = {}

function assert.equal(v1, v2, depth)
    if not depth then
        depth = 1
    end
    local t1 = type(v1)
    local t2 = type(v2)
    if (t1 ~= t2) then
        error('got different values: "' .. toPrettyPrint(v1) ..'" : "'..  toPrettyPrint(v2) .. '"', depth)
    end

    if (v1 ~= v2) then
        if (t1 == "table") then
            -- recurse into both tables
            for k, v in pairs(v1) do
                
                assert.equal(v1[k], v2[k], depth + 1)
            end
            for k, v in pairs(v2) do
                if v1[k] == nil and v2[k] ~= nil then
                    error('v1 is missing value for key '.. k, depth)
                end
            end
        else
            error('got different values: "' .. toPrettyPrint(v1) ..'" : "'..  toPrettyPrint(v2) .. '"', depth)
        end
    end    
end

function assert.notNil(v, msg)
    if not v then
        if not msg then
            msg = ""
        else
            msg = ": " .. msg
        end
        error("expected value to not be nil" .. msg, 2)
    end
end

local types = {
    ["nil"]  = "nil",
    number = "number",
    string = "string",
    boolean = "boolean",
    table = "table",
    ["function"] = "function",
    thread = "thread",
    userdata = "userdata"
}

function assert.isTypeString(str)
    local r = types[str]
    if (r == nil) then
        error("expected a name of a type, got " .. toPrettyPrint(str))
    end

end

--  "nil" | "number" | "string" | "boolean" | "table" | "function" | "thread" | "userdata"
function assert.isType(t1, v, msg)
    msg = (msg or "")
    assert.isTypeString(t1)
    local t2 = type(v)
    if t1 ~= t2 then
        error(msg ..": expected type: " .. t1 .. " but found " .. t2 .. ' with value ' .. toPrettyPrint(v))
    end
end

function assert.todo(str)
    error("TODO: " ..str)
end

return assert
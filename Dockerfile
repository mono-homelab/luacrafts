FROM alpine
RUN apk update && apk add build-base lua5.3-dev lua5.3 git wget curl unzip openssl openssl-dev

# install latest version of luarocks to fix git / https issues with installing lua-nums
WORKDIR /root
ADD https://luarocks.org/releases/luarocks-3.8.0.tar.gz .
RUN tar zxpf luarocks-3.8.0.tar.gz
WORKDIR /root/luarocks-3.8.0
RUN ./configure && make install

RUN mkdir /code
ADD ./Makefile /code
WORKDIR /code
RUN make setup

ADD . /code/


require('./util')
require('./encoder')
local assert = require('./mlib/assert')

testSuite("encoder", function()

    doTest("basic encoding", function() 
        assert.equal(encode(5), fromhex('0305000000'))
        assert.equal(encode("abc"), fromhex('1003000000616263'))
        assert.equal(decode(fromhex('00')), nil)
        assert.equal(decode(fromhex('100100000061')), 'a')
        assert.equal(decode(fromhex('1003000000616263')), 'abc')
        assert.equal(encode("value_str"), fromhex('100900000076616C75655F737472'))
        assert.equal(encode({ arr = { 1, 2, 3}}), fromhex('1112000000010301000000010302000000010303000000'))
        
    end)

    doTest("basic encode / decode", function()
        local testData = {
            5,
            100,
            10.5,
            3.333,
            6.666,
            "a",
            "abc",
            "ascii"
        }
        for k,v in pairs(testData) do
            local buf = encode(v)
            -- logInfo(k .. " : " .. tohex(buf))
            local dec = decode(buf)
            -- assert v == dec for basic types
            assert.equal(v, dec)
           
        end
    end)

    doTest("encode / decode tables", function()
        local tables = {
            {
                key_str = "value_str"
            },
            {
                num_str = 5,
                key_str = "value_str",
                other_key_str = "value_str2"
            },
            {
                num = 5,
                foo = 5.32,
                nil_val = nil, -- TODO how to handle nil values?
                key_str = "value_str"
            },
            {
                num = 5,
                foo = 5.32,
                nexted = {
                    qwerty = 'uiop;',
                    num = 5,
                    nil_val = nil,
                },
                key_str = "value_str"
            },
            {
                arr = {
                    1,
                    2,
                    3,
                    4,
                }
            }
        
        }
        for k,v in pairs(tables) do
            local buf = encode(v)
            -- logInfo( k .. " : " .. tohex(buf))
            -- should load tables without schema
            -- will be missing keys
            local dec = decode(buf)
            -- TODO validate dec
        end
    end)

    doTest("encode with schema", function()
        local buf = encode({
            bar1 = 'stuff',
            num1 = 5,
            num2 = 3.3333,
     
         }, testSchema1)
         local expected = "111B00000001100500000073747566660203050000000304ED0DBE3099AA0A40"
         -- assert.equal(tohex(buf), expected)
         -- TODO test expected value, keys won't always be in same order
    end)

end)
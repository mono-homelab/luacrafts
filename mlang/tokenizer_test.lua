require("io")
require('mlang/tokenizer')


testSuite("tokenizer", function()
    doTest("can tokenize self", function()
        -- TODO tokenizer breaks if file doesn't end with empty line
        file = io.open("mlang/tokenizer.lua", "r")
        tokens = {}

        for token in lang_tokenize(file) do
            print(toPrettyPrint(token))
            table.insert(tokens,token)
        end
    end)
end)
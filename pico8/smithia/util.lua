function draw_chart(x,y, max, title, data)
    border = 7
    bg = 0
    fg = 8
    -- print(str, x, y)
    -- color()
    

    rect(x,y,x+21,y+11, border)
    for k, v in pairs(data) do
        x2 = 21 - k + x
        height = min(10, v / max * 10)
        y2 = y + (10 - height) + 1
        line(x2,y + 10, x2, y2,fg)
    end 
end

-- TODO map generator

function new_tile(x,y,ttype)
    -- x,y is on the game map
    local tile = {
        x=x,
        y=y,
        ttype=ttype
    }
    return tile
end

function new_island_map()
    local imap = {}
    for y = 1,16 do
        imap[y] = {}
        for x = 1,16 do
            local tile = new_tile(x,y,tile_type.air)
            imap[y][x] = tile

        end
    end
    return imap
end

function isWalkable(x,y)
    return fget(mget(x,y),0)
end

function isWall(x,y)
    return fget(mget(x,y),1)
end

function isCeiling(x,y)
    return fget(mget(x,y),2)
end
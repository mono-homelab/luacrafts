-- globals
island_map = {} -- 2d map of tiles
entities = {}

drawFNs = {}
updateFNs = {}

function _init()
    -- reset debug output
    printh("INIT", debuglogfile, true)

    island_map = new_island_map()
    music(0)

    -- test map is 16x16
    for y = 1,16 do
        for x = 1,16 do
            ttype = mget(x,y)
            if ttype == etype_player then
                new_player(x*8,y*8)
                ttype = 0
            end
            island_map[y][x].ttype = ttype
        end
    end

    function updateIsland()
        -- update state of the map
        for y = 1,16 do
            for x = 1,16 do
                local tile = island_map[y][x]
                mset(x,y,tile.ttype)
            end
        end
    end
    
    function drawIsland()
        -- draw demo island to screen
        map(0,0,0,0,16,16)
    end
    add(updateFNs,updateIsland)
    add(drawFNs,drawIsland)

end

function _update60()
    for fn in all(updateFNs) do
        fn()
    end
end

function _draw()
    cls()

    for fn in all(drawFNs) do
        fn()
    end
end

function dprint(str)
    printh(str, debuglogfile)
end

function stub() end

## pico 8 notes

- cheatsheet
  - https://www.lexaloffle.com/bbs/files/16585/PICO-8_CheatSheet_0111Gm_4k.png

first 2 comments on a cart is the label

```
-- label title
-- label second line
```

### special callbacks

```
_init()
_update()
_update60()
_draw()
```

## fns

`assert(condition, message)`

## commands

- `INFO` print cart info like 'tokens'

## controls

- ctrl-p opens cpu graph

# WIP

- learning my way around ethereum internals
- https://ethereum.org/en/developers/docs/evm/opcodes/

# References

- https://en.wikipedia.org/wiki/SHA-3
- https://pkg.go.dev/golang.org/x/crypto/sha3#NewLegacyKeccak256
- https://eth.wiki/en/fundamentals/rlp
- https://github.com/ethereum/devp2p/blob/master/rlpx.md
- https://github.com/ethereum/devp2p/blob/master/caps/eth.md
- https://github.com/ethereum/go-ethereum
local Vector = require("mlib/vector")
local u64 = require("nums.uintb").u64
local assert = require('mlib/assert')

-- Ethereum doesn't use normal sha3, but instead  legacyKeccak hashing algorithm

-- https://ethereum.stackexchange.com/questions/30369/difference-between-keccak256-and-sha3

-- ethereum uses the legacy sha3 hashing function with dsByte 0x01, rather than 0x06

-- https://cs.opensource.google/go/x/crypto/+/86341886:sha3/sha3.go
-- proper sha3 implementation (not legacy) https://github.com/user-none/lua-hashings/blob/master/hashings/keccak.lua
-- https://github.com/chfast/ethash/blob/ec47e394dd6bc02cf8348212cb2bc3e6cb3fbc6c/lib/keccak/keccak.c

-- legacy keccak 256
-- rate 136
-- dsSizeLen 32
-- dsByte 0x01

-- legacy keccak 512
-- rate 72
-- dsSizelen 64
-- dsbyte 0x01

-- constants
-- https://cs.opensource.google/go/x/crypto/+/86341886:sha3/keccakf.go
local rc = Vector.of({
    '0x0000000000000001',
	'0x0000000000008082',
	'0x800000000000808A',
	'0x8000000080008000',
	'0x000000000000808B',
	'0x0000000080000001',
	'0x8000000080008081',
	'0x8000000000008009',
	'0x000000000000008A',
	'0x0000000000000088',
	'0x0000000080008009',
	'0x000000008000000A',
	'0x000000008000808B',
	'0x800000000000008B',
	'0x8000000000008089',
	'0x8000000000008003',
	'0x8000000000008002',
	'0x8000000000000080',
	'0x000000000000800A',
	'0x800000008000000A',
	'0x8000000080008081',
	'0x8000000000008080',
	'0x0000000080000001',
	'0x8000000080008008',
})

local big_rc = rc:map(function(str) 
    return u64(str)
end)

function keccakF1600(a)
    -- NB. does this need to be 25?
    for i = 1, 24 do
        -- TODO
        -- load block
        -- permute theta
        -- permute rho
        -- permute chi
        -- permute iota
    end
end

-- Keccak class

local Keccak = {}

function Keccak:new256()
    local hash = setmetatable({}, {__index = Keccak })

    hash.rate = 136
    hash.dsSize = 32
    hash.dsByte = 0x01
    hash.data = ""
    hash.sponge = Vector:new()
    for i = 1,25 do
        hash.sponge:push(u64(0))
    end
    return hash
end

function Keccak:new512()
    local hash = setmetatable({}, { __index = Keccak })
    hash.rate = 72
    hash.dsSize = 64
    hash.dsByte = 0x01
    hash.data = ""
    hash.sponge = Vector:new()

    return hash
end

function Keccak:size()
    return self.dsSize
end

function Keccak:copy()

    local hash = setmetatable({}, {__index = Keccak })
    hash.rate = self.rate
    hash.dsSize = self.dsSize
    hash.data = self.data
    hash.sponge = self.sponge:copy()
    
    return hash
end

-- takes in a byte array as a str
function Keccak:write(byteStr)
    assert.isType("string", byteStr)

    -- self.data may have lingering data when #data < rate
    self.data = self.data .. byteStr
    
    -- xor in 'rate' bytes
    -- apply keccakF1600 permutation
    -- TODO

end

function Keccak:digest()

    local copy = self:copy()

    -- check for remaining data in _data
    -- write trailer w/ padding if so
    -- dsByte .. (0x00 * rate - #self.data - 2) .. 0x80
    -- if no padding required
    -- dsByte | 0x80

    local data = ""
    if #copy.data == copy.rate - 1 then
        data = string.char(copy.dsByte | 0x80)
    else
        data = string.char(0x06) .. string.rep(string.char(0), copy.rate - #copy.data - 2) .. string.char(0x80)
    end
    self:write(data)

    -- self.data should be empty at this point
    -- assert.equal(#self.data, 0)

    -- squeeze out the sponge
    local sum = {}

    for i=1,copy.dsSize//8 do
        sum[i] = copy.sponge[i]:swape():asbytestring()
    end

    return table.concat(sum)
end

return Keccak
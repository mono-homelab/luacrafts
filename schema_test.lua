require('./schema')

testSuite("schema", function()

    doTest("schema validating", function()
        local testSchema1 = {
            type = 'foo',
            fields = {
                {
                    name = 'bar1',
                    repeated = false,
                    type = 'string',
                    id = 5,
                },
                {
                    name = 'num1',
                    repeated = false,
                    type = 'integer',
                    id = 10,
                },
                {
                    name = 'num2',
                    repeated = false,
                    type = 'double',
                    id = 18,
                }
            }
        }
        validateSchema(testSchema1)
    end)

end)